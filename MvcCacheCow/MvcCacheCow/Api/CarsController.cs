﻿
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcCacheCow.Api
{
    public class CarsController:ApiController
    {
        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get()
        {
            var cars = new List<Car>
            {
                new Car
                {
                    Id = 1,
                    Name = "name 1"
                },
                new Car
                {
                    Id = 2,
                    Name = "name 2"
                },
                new Car
                {
                    Id = 3,
                    Name = "name 3"
                },
                new Car
                {
                    Id = 4,
                    Name = "name 4"
                }
            };
            return Request.CreateResponse(HttpStatusCode.OK, cars);
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var car =
                new Car
                {
                    Id = 1,
                    Name = "name 1"
                };
            return Request.CreateResponse(HttpStatusCode.OK, car);
        }

        [System.Web.Http.HttpPut]
        public HttpResponseMessage Put(int id, Car model)
        {
            var car =
                new Car
                {
                    Id = model.Id,
                    Name = model.Name
                };
            return Request.CreateResponse(HttpStatusCode.OK, car);
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage Post(Car model)
        {
            var car =
                new Car
                {
                    Id = model.Id,
                    Name = model.Name
                };
            return Request.CreateResponse(HttpStatusCode.OK, car);
        }
    }

    public class Car
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}